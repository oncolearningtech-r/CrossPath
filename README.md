# CrossPath
    Crossing Pathways

# Instruções 
1. Clone o repositório (é recomendado que clone para a pasta Documentos ou sua pasta home, defalut working directory, do RStudio)
2. faça o download do arquivo a seguir:
    [Download](https://drive.google.com/file/d/1OJZnXrjhvUwIa7-6VvUk7j_-yvnmmq7U/view?usp=sharing)
2. Cole o arquivo no diretório "data/deseq/"

3. Execute por completo o arquivo "00_start.R"; 
4. Ele abrirá os dois proximos arquivos a serem executador; 

5. Selecione a via no site do [KEGG PATHWAY](https://www.genome.jp/kegg/pathway.html), utilize o código da via disponibilizado no item 'Pathway Entry'.
    - Para vias do tipo "HSA" execute o codigo com final 1;
    - Para vias do tipo "MAP" execute o codigo com final 2
        > Obs.: para vias do tipo MAP é necessário usar seu código "KO pathway" disponível no final da página 'Pathway Entry'.

***Thayane Batista***
***Oncolearning Tecnology***
***2020***
